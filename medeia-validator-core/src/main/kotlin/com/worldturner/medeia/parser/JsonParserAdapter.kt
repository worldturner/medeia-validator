package com.worldturner.medeia.parser

interface JsonParserAdapter {
    fun parseAll()
}